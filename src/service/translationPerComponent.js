import i18n from 'i18next';
import { resolve } from 'url';


export const init = ({ lang, json }) => {
    console.log("json: ", json);
    // return i18n.createInstance({
    //     lng: lang,
    //     resources: {
    //         [lang]: { json }
    //     }
    // });

    return new Promise(async (resolve, reject) => {
        i18n.createInstance({
            lng: lang,
            resources: {
                [lang]: { json }
            }
        }, (err, t) => {
            if (err) return reject(err);
            resolve(t);
        });
    });
};
