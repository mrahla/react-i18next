import React, { useState, useEffect } from "react";
import { init as initTranlation } from '../../service/translationPerComponent';
import styled from "styled-components";
import { HeaderText, SubHeaderText } from "../../styles/commonStyles";
import { useTranslation } from 'react-i18next';


const ResourceHeaderText = styled(HeaderText)`
  color: #686868;
  margin-bottom: 3px;
`;

export default function ResourcesHeader() {
    //const { t } = global;
    let trans = () => { };

    useEffect(() => {
        var data = require('./ResourcesHeader/locales/en-US/strings.json');
        console.log(data);
        const fetchTranslate = async () => {
            //const { t, i18n } = useTranslation('', { a });
            trans = await initTranlation({ lang: "en-US", json: data });
            console.log("t:", trans);
            console.log("title:", trans('TITLE'));
        }
        fetchTranslate();
    }, []);

    return (
        <>
            <ResourceHeaderText>{trans("TITLE")}aaaa</ResourceHeaderText>
            <SubHeaderText>{trans("SUBTITLE")}</SubHeaderText>
        </>
    );
}
